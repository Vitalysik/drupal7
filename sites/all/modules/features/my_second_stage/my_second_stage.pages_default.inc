<?php
/**
 * @file
 * my_second_stage.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function my_second_stage_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__panel_context_021de30b-02bf-4129-ad53-3f858d3cadef';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Page',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'my_page' => 'my_page',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'c510ebc9-48c3-4205-a3cc-0f9aac0df0b9';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-d5bb77c6-6df7-4bad-8633-e04e00a698ce';
    $pane->panel = 'bottom';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_custom_text';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd5bb77c6-6df7-4bad-8633-e04e00a698ce';
    $display->content['new-d5bb77c6-6df7-4bad-8633-e04e00a698ce'] = $pane;
    $display->panels['bottom'][0] = 'new-d5bb77c6-6df7-4bad-8633-e04e00a698ce';
    $pane = new stdClass();
    $pane->pid = 'new-cd567e02-985a-467f-86ae-9ca16475ac04';
    $pane->panel = 'bottom';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_number';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'inline',
      'formatter' => 'number_integer',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'thousand_separator' => '',
        'prefix_suffix' => 1,
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'cd567e02-985a-467f-86ae-9ca16475ac04';
    $display->content['new-cd567e02-985a-467f-86ae-9ca16475ac04'] = $pane;
    $display->panels['bottom'][1] = 'new-cd567e02-985a-467f-86ae-9ca16475ac04';
    $pane = new stdClass();
    $pane->pid = 'new-687d791a-1b2a-4ff4-972f-c7f0edb50033';
    $pane->panel = 'left';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_pictures';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => 'large',
        'image_link' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '687d791a-1b2a-4ff4-972f-c7f0edb50033';
    $display->content['new-687d791a-1b2a-4ff4-972f-c7f0edb50033'] = $pane;
    $display->panels['left'][0] = 'new-687d791a-1b2a-4ff4-972f-c7f0edb50033';
    $pane = new stdClass();
    $pane->pid = 'new-4949c77c-ebcd-45e1-8835-a653e0dc6149';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4949c77c-ebcd-45e1-8835-a653e0dc6149';
    $display->content['new-4949c77c-ebcd-45e1-8835-a653e0dc6149'] = $pane;
    $display->panels['right'][0] = 'new-4949c77c-ebcd-45e1-8835-a653e0dc6149';
    $pane = new stdClass();
    $pane->pid = 'new-d84566a8-73a8-468e-aa39-cceb74942ba9';
    $pane->panel = 'top';
    $pane->type = 'node_title';
    $pane->subtype = 'node_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 1,
      'markup' => 'h2',
      'id' => '',
      'class' => '',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd84566a8-73a8-468e-aa39-cceb74942ba9';
    $display->content['new-d84566a8-73a8-468e-aa39-cceb74942ba9'] = $pane;
    $display->panels['top'][0] = 'new-d84566a8-73a8-468e-aa39-cceb74942ba9';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-d84566a8-73a8-468e-aa39-cceb74942ba9';
  $handler->conf['display'] = $display;
  $export['node_view__panel_context_021de30b-02bf-4129-ad53-3f858d3cadef'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function my_second_stage_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'genres';
  $page->task = 'page';
  $page->admin_title = 'Genres';
  $page->admin_description = '';
  $page->path = 'genres';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Genres',
    'name' => 'main-menu',
    'weight' => '5',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_genres__panel';
  $handler->task = 'page';
  $handler->subtask = 'genres';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'middle' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Genres';
  $display->uuid = '731368ad-acc5-4993-9987-21caa9fe862b';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-edf89373-ac83-4dc4-904c-caae9bba264e';
    $pane->panel = 'left';
    $pane->type = 'views';
    $pane->subtype = 'list_of_genres';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'page',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'edf89373-ac83-4dc4-904c-caae9bba264e';
    $display->content['new-edf89373-ac83-4dc4-904c-caae9bba264e'] = $pane;
    $display->panels['left'][0] = 'new-edf89373-ac83-4dc4-904c-caae9bba264e';
    $pane = new stdClass();
    $pane->pid = 'new-e57528c9-c4f5-4cd1-a0a2-5713b39c752c';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'views-popular_reviews-block';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e57528c9-c4f5-4cd1-a0a2-5713b39c752c';
    $display->content['new-e57528c9-c4f5-4cd1-a0a2-5713b39c752c'] = $pane;
    $display->panels['right'][0] = 'new-e57528c9-c4f5-4cd1-a0a2-5713b39c752c';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['genres'] = $page;

  return $pages;

}
