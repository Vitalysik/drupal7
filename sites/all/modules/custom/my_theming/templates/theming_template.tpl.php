<?php
/*
 * $file
 * Custom theming template.
 */
?>

<div class="custom-template-wrapper">
  <h2 class="title">
    <?php print $title; ?>
  </h2>
  <div class="body">
    <strong>
      <?php print $body; ?>
    </strong>
  </div>
</div>
